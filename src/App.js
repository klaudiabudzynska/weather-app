import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

import LoginController from './loginController/LoginController';
import AdminDashboard from './admin/AdminDashboard';
import UserDashboard from './user/UserDashboard';
import { ProtectedRoute } from './loginController/ProtectedRoute';

function App() {
  return (
    <Router>
      <Switch>
        <ProtectedRoute path="/admin/dashboard" component={AdminDashboard}/>
        <ProtectedRoute path="/user/:name/dashboard" component={UserDashboard}/>
        <Route path="/" component={LoginController} />
        <Route path="/*">
          <div>404 - not found</div>
        </Route>
      </Switch>
    </Router>
  );
}

export default App;

import React, { Component } from 'react';
import LoginForm from '../loginForms/LoginForm';
import Register from '../register/Register';
import { Tabs, Tab } from 'react-bootstrap';
import './loginController.css';

export default class LoginController extends Component {
  render(){
    return (
      <div className="login-controller">
        <Tabs defaultActiveKey="profile" id="tabs">
          <Tab eventKey="signin" title="Sign in">
            <LoginForm />
          </Tab>
          <Tab eventKey="signup" title="Sign up">
            <Register />
          </Tab>
        </Tabs>
      </div>
    );
  }
  
}

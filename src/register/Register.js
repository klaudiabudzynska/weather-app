import React, { Component } from 'react';
import axios from 'axios';
import { Button, Form } from 'react-bootstrap';

export default class Register extends Component {
  constructor(){
    super();

    this.state = {
      name: '',
      email: '',
      location: '',
      formMessage: ''
    }

    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handleLocationChange = this.handleLocationChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(event) {
    const {name, email, location} = this.state
    axios.post('http://localhost:8080/users/signup', {name: name, email: email, location: location})
    .then(res => {
      this.setState({ formMessage: res.data.message })
    });

    event.preventDefault();
  }

  handleNameChange(event) {
    const name = event.target.value;
    this.setState({
      name: name
    });
  }

  handleEmailChange(event) {
    const email = event.target.value;
    this.setState({
      email: email
    });
  }

  handleLocationChange(event) {
    const location = event.target.value;
    this.setState({
      location: location
    });
  }

  render() {
    return (
      <Form onSubmit={this.handleSubmit}>
        <Form.Group>
          <Form.Label>Name</Form.Label>
          <Form.Control 
            type="text" 
            value={this.state.name} 
            onChange={this.handleNameChange} 
            placeholder="Enter your name"
          />
        </Form.Group>
        <Form.Group>
          <Form.Label>Email</Form.Label>
          <Form.Control 
            type="email" 
            value={this.state.email} 
            onChange={this.handleEmailChange} 
            placeholder="Enter your email"
          />
        </Form.Group>
        <Form.Group>
          <Form.Label>Location</Form.Label>
          <Form.Control 
            type="text" 
            value={this.state.location} 
            onChange={this.handleLocationChange} 
            placeholder="Enter location"
          />
        </Form.Group>
        <Button type="submit">Log in</Button>
        <Form.Group>
          <Form.Text>
            {this.state.formMessage}
          </Form.Text>
        </Form.Group>
      </Form>
    );
  }
}

import React, { Component } from 'react';
import { Redirect } from "react-router-dom";
import axios from 'axios';
import auth from '../Auth';
import { Button, Form } from 'react-bootstrap';

export default class AdminForm extends Component {
  constructor(){
    super();

    this.state = {
      userType: 'user',
      name: '',
      password: '',
      formMessage: '',
      redirect: 0
    }

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleNameChange = this.handleNameChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.handleSelect = this.handleSelect.bind(this);
  }

  handleSubmit(event) {
    const {name, password} = this.state
    axios.post(`http://localhost:8080/users/signin/${this.state.userType}`, {name: name, password: password})
    .then(res => {
      res.data.logInStatus === 1 ? auth.logIn(() => { this.setState({ redirect: 1 }) })
      :
      this.setState({ formMessage: res.data.message })
    });

    event.preventDefault();
  }

  handleNameChange(event) {
    const name = event.target.value;
    this.setState({
      name: name
    });
  }

  handlePasswordChange(event) {
    const password = event.target.value;
    this.setState({
      password: password
    })
  }

  handleSelect(event) {
    const userType = event.target.value;
    this.setState({
      userType: userType
    })
  }

  render(){
    const { redirect, userType } = this.state;

    if (redirect) {
      if(userType === 'admin') return <Redirect to='/admin/dashboard'/> 
      else if(userType === 'user') return<Redirect to={`/user/${this.state.name}/dashboard`}/>
    }

    return (
      <Form onSubmit={this.handleSubmit}>
        <Form.Group controlId="exampleForm.ControlSelect1">
          <Form.Label>Choose user type</Form.Label>
          <Form.Control 
            as="select"
            onChange={this.handleSelect}
          >
            <option defaultValue value="user">User</option>
            <option value="admin">Admin</option>
          </Form.Control>
        </Form.Group>
        <Form.Group>
          <Form.Label>Name</Form.Label>
          <Form.Control 
            type="text" 
            value={this.state.name} 
            onChange={this.handleNameChange} 
            placeholder="Enter your name"
          />
        </Form.Group>
        <Form.Group>
          <Form.Label>Password</Form.Label>
          <Form.Control 
            type="password" 
            value={this.state.password} 
            onChange={this.handlePasswordChange} 
            placeholder="Enter your password"
          />
        </Form.Group>
        <Button type="submit">Log in</Button>
        <Form.Group>
          <Form.Text>
            {this.state.formMessage}
          </Form.Text>
        </Form.Group>
      </Form>
    )
  }
}
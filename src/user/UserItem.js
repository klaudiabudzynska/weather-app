import React, { Component } from 'react';
import axios from 'axios';
import { Button, Row, Col } from 'react-bootstrap';
import './userItem.css';

export default class UserItem extends Component {
  handlePasswordSet(name){
    axios.post(`http://localhost:8080/users/${name}/password`)
    .then(res => {
      console.log(res);
    });
  }
  render() {
    const {name, email, location} = this.props;
    return (
      <Row className="user-item"> 
        <Col>name: {name}</Col> 
        <Col>email: {email}</Col> 
        <Col>location: {location}</Col> 
        <Col xs={2} className="user-item__button">
          <Button onClick={()=>this.handlePasswordSet(name)}>set password</Button>
        </Col>
      </Row>
    );
  }
}

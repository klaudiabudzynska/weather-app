import React, { Component } from 'react';
import Geocode from "react-geocode";
import { Container, Row, Col } from 'react-bootstrap';
import './userDashboard.css';

export default class UserDashboard extends Component {
  constructor(){
    super();

    this.state = {
      data: {
        name: '',
        email: '',
        location: '',
        humidity: 0,
        temperature: 0
      }
    }
  }

  componentDidMount(){
    const { name } = this.props.match.params

    fetch(`http://localhost:8080/users/${name}`)
    .then(user => user.json())
    .then(user => {
      this.setState({
        name: name,
        email: user.email,
        location: user.location
      }, () => {
        Geocode.setApiKey(process.env.REACT_APP_API_KEY);
        Geocode.fromAddress(this.state.location).then(
          response => {
            const { lat, lng } = response.results[0].geometry.location;

            fetch(`https://api.met.no/weatherapi/locationforecast/1.9/.json?lat=${lat}&lon=${lng}`)
            .then(res => res.json())
            .then(res => {
              const { humidity, temperature } = res.product.time[0].location;

              this.setState({
                humidity: humidity.value,
                temperature: temperature.value
              })
            })
          },
          error => {
            console.error(error);
          }
        );
      })
    })

    
  }

  render() {
    const { name, email, location, humidity, temperature} = this.state;

    return (
      <Container className="user-dashboard"> 
        <Row className="user-dashboard__user">
          <Col xs={3} className="user-dashboard__title">User data:</Col>
          <Col xs={9}>
            <Row>
              <Col xs={3}>Name:</Col>
              <Col xs={9}>{name}</Col>
            </Row>
            <Row>
              <Col xs={3}>Email:</Col>
              <Col xs={9}>{email}</Col>
            </Row>
          </Col>
        </Row>
        <Row className="user-dashboard__weather">
          <Col xs={3} className="user-dashboard__title">Weather in {location}</Col>
          <Col xs={9}>
            <Row>
              <Col xs={3}>Humidity:</Col>
              <Col xs={9}>{humidity}%</Col>
            </Row>
            <Row>
              <Col xs={3}>Temperature:</Col>
              <Col xs={9}>{temperature}<sup>o</sup>C</Col>
            </Row>
          </Col>
        </Row>
        
      </Container>
    );
  }
}

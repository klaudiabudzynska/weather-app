import React, { Component } from 'react';
import UserItem from '../user/UserItem';
import Container from 'react-bootstrap/Container';
import './adminDashboard.css';

export default class AdminDashboard extends Component {
  constructor(){
    super();

    this.state ={
      users: [],
    }
  }

  componentDidMount(){
    fetch('http://localhost:8080/users')
    .then(users => users.json())
    .then(users => {
      this.setState({ 
        users: users
      })
    })
  }

  render() {
    const { users } = this.state;
    let usersList = [];

    users.forEach((user, i) => {
      usersList.push(<UserItem key={i} {...user}/>)
    })
    return (
      <Container className='admin-dashboard'> 
        <h1 className='admin-dashboard__title'>Users:</h1> 
        {usersList} 
      </Container>
    );
  }
}

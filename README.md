### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm install`

Install all dependencies

### `npm run server`

Launches the server

### `Create .env file with this content:`

NODE_ENV=development
EMAIL=youremailadress
PASSWORD=youremailpassword
REACT_APP_API_KEY=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

### `Create users.json file in ./server directory with this content:`

[{"admin":"true","name":"admin","password":"123"}]

Name and password values could be any
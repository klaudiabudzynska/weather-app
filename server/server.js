const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const fs = require('fs');
const nodemailer = require('nodemailer');
const dotenv = require('dotenv');

dotenv.config();

const port = 8080;
const usersFile = 'server/users.json'
const app = express();
const transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: process.env.EMAIL,
    pass: process.env.PASSWORD
  },
  tls: { 
    rejectUnauthorized: false 
  }
});

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

app.post('/users/signup', (req, res) => {
  const { name, email, location } = req.body;

  fs.readFile(usersFile, 'utf8', (err, data) => {
    users = JSON.parse(data);
    users.push({
      admin: false,
      name: name, 
      email: email, 
      location: location,
      password: ""
    });

    json = JSON.stringify(users);
    fs.writeFile(usersFile, json, 'utf8', () => {
      return res.json({
        logInStatus: 1,
        message: 'Wait for admin to give you password'
      });
    });
  });
}); 

app.post('/users/signin/admin', (req, res) => {
  const { name, password } = req.body;

  fs.readFile(usersFile, 'utf-8', (err, data) => {
    users = JSON.parse(data);

    if (!name || !password) {
      return res.json({
        logInStatus: 0,
        message: "Name or Password required."
      });
    }

    let selectedUser;
    users.forEach(user => {
      if(user.admin){
        selectedUser = user;   
      }
    });

    if(!selectedUser){
      return res.json({
        logInStatus: 0,
        message: 'Admin not found'
      });
    }

    if (name !== selectedUser.name || password !== selectedUser.password) {
      return res.json({
        logInStatus: 0,
        message: "Name or Password is Wrong."
      });
    }
  
    return res.json({
      logInStatus: 1,
      message: 'Login successful'
    });

  }); 
});

app.post('/users/signin/user', (req, res) => {
  const { name, password } = req.body;

  fs.readFile(usersFile, 'utf-8', (err, data) => {
    users = JSON.parse(data);

    if (!name || !password) {
      return res.json({
        logInStatus: 0,
        message: "Name or Password required."
      });
    }
    let selectedUser;
    users.forEach(user => {
      if(user.name === name){
        selectedUser = user;                
      }
    });

    if(!selectedUser){
      return res.json({
        logInStatus: 0,
        message: 'User not found'
      });
    }

    console.log(selectedUser);

    if (name !== selectedUser.name || password !== selectedUser.password) {
      return res.json({
        logInStatus: 0,
        message: "Name or Password is Wrong."
      });
    }
  
    return res.json({
      logInStatus: 1,
      message: 'Login successful'
    });

  }); 
});

app.get('/users', (req, res) => {
  fs.readFile(usersFile, 'utf-8', (err, data) => {
    const users = JSON.parse(data).filter(item => item.admin === false);
    res.json(users);
  })
})

app.get('/users/:name', (req, res) => {
  const { name } = req.params;

  fs.readFile(usersFile, 'utf-8', (err, data) => {
    const users = JSON.parse(data);

    users.forEach(user => {
      if(user.name === name) {
        const { email, location } = user;

        res.json({
          email: email,
          location: location
        });
      }
    })
  })
})

app.post('/users/:name/password', (req, res) => {
  const { name } = req.params;

  fs.readFile(usersFile, 'utf-8', (err, data) => {
    const users = JSON.parse(data);

    let randomPassword = Math.random().toString(36).toUpperCase().slice(-8);
    let arr = randomPassword.split();
    arr.forEach((element, i) => {
      var j = Math.floor(Math.random() * (i + 1));
      var tmp = arr[i];
      arr[i] = arr[j];
      arr[j] = tmp;
    });
    randomPassword = arr.join("");
    console.log("password", randomPassword);

    users.forEach(user => {
      if(user.name === name){
        console.log(user);
        user.password = randomPassword;
        json = JSON.stringify(users);

        let mailOptions = {
          from: process.env.EMAIL,
          to: user.email,
          subject: 'Your new password',
          text: `This is your new password: ${randomPassword}`
        };
        
        transporter.sendMail(mailOptions, (error, info) => {
          if (error) {
            console.log(error);
            res.send(error);
          } else {
            console.log("Message sent: " + info.message);
            res.send('Email sent: ' + info.response);
          }
          
          fs.writeFile(usersFile, json, 'utf8', () => {
            console.log('file edit successful')
          });
        });
      }
    })
  })
})

app.listen(port, () =>
  console.log(`Express server is running on localhost:${port}`)
);